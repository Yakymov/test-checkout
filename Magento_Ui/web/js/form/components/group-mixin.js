define(function () {
    'use strict';

    var mixin = {

        viceVersa: function (text){
            let string = text.split('').reverse().join('');
            return string;
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
