var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Magento_Checkout/js/view/shipping-mixin': true
            },
            'Magento_Checkout/js/view/form/element/email': {
                'Magento_Checkout/js/view/form/element/email-mixin': true
            },
            'Magento_Ui/js/form/element/abstract': {
                'Magento_Ui/js/form/element/abstract-mixin': true
            },
            'Magento_Ui/js/form/components/group': {
                'Magento_Ui/js/form/components/group-mixin': true
            }
        }
    }
};