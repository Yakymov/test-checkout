define(
    [
        'mage/url'
    ]
    ,function (url) {
    'use strict';

    var mixin = {

        /**
         * Set shipping information handler
         */
        validateShippingInformation: function () {
            window.location.href = url.build('checkout/cart');
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
