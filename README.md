*﻿Magento Custom theme Test/Test inherits Magento/luma theme*


1 - Disable the required fields via layout. Go to the checkout_index_index.xml layout and disable fields (Company and City) http://prntscr.com/vlaj5b


2 - Go to the checkout_index_index.xml and find the item with name="component". In our case, we have to extend (by 'mixin') the following components: 'Magento_Checkout/js/view/shipping',
'Magento_Checkout/js/view/form/element/email', 'Magento_Ui/js/form/element/abstract' , 
'Magento_Ui/js/form/components/group', now we override 'template'.

I added a new method 'viceVersa'.

Results:
- Requirejs-config.js - http://prntscr.com/vlbhbt

- it’s necessary to add a new method to the template - http://prntscr.com/vlbi1b

- mixins - http://prntscr.com/vlbioz


3 - 

Variant №1 - Find in the file Magento_Checkout/web/template/shipping.html <button 'Next'> and replace the 'button' tag with 'a' and add href="/checkout/cart". 
That is a nuts way! 
In the next variant is about a js way.

_______________________________________

Variant №2 - Create shipping-mixin.js and extend existing method "triggerShippingDataValidateEvent" or "validateShippingInformation". 

If we use 'validateShippingInformation' method:

The user will be immediately directed to the shopping cart page and will not see an error in the validation field.

_______________________________________

If we use  triggerShippingDataValidateEvent - an error will be displayed and the user will be redirected to the shopping cart page.

Requirejs-config.js - 

'Magento_Checkout/js/view/shipping': {
	'Magento_Checkout/js/view/shipping-mixin': true
}


- mixin - http://prntscr.com/vlbtna


=====================================

Results: http://prntscr.com/vlc2d2

Note: English is not my native language so I can make mistakes in the description, that is why I attached theme files.

